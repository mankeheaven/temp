


var swiper1 = new Swiper(".swiper-container", {
  spaceBetween: 0,
  pagination: ".swiper-dot",
  nextButton: ".swiper-container .swiper-next",
  prevButton: ".swiper-container .swiper-prev",
  loop: true,
  effect: "fade",
  paginationClickable: true,
  autoplay: 5000,
  // paginationType: 'fraction',
  autoplayDisableOnInteraction: false,
})
